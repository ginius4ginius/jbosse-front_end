# Getting Started

### Reference Documentation
Ligne de commande pour éxécuter les tests d'integration depuis la racine du projet :

* npm run integration-tests-local

Lien Postam de la documentation de la collection de tests
* [https://documenter.getpostman.com/view/10278066/SzmfZHdj]()
