import {ValidatorFn, AbstractControl, FormControl, FormGroup} from "@angular/forms";


export class CustomValidators {


  /** Validation nom du contact par REGEX **/
  public static forbiddenNameValidator(nameRe: RegExp): ValidatorFn {
    return (control: AbstractControl): { [key: string]: any } | null => {
      const forbidden = nameRe.test(control.value);
      return forbidden ? {'forbiddenName': {value: control.value}} : null;
    };
  }

  /** Validation email du contact par REGEX **/
  public static emailValidator(): ValidatorFn {
    return (control: AbstractControl) => {
      let isValid = /^[_a-z0-9]+(\.[_a-z0-9]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/.test(control.value);
      if (isValid) {
        return null;
      } else {
        return {
          'emailValidator': {value: control.value}
        };
      }
    }
  }

  /** Validation du mot de passe delta sa confirmation**/
  public static passwordMatcher(controlPassword: string, matchingControlPassword: string) {
    return (formGroup: FormGroup) => {
      const control = formGroup.controls[controlPassword];
      const matchingControl = formGroup.controls[matchingControlPassword];

      if (matchingControl.errors && !matchingControl.errors.mustMatch) {
        // return if another validator has already found an error on the matchingControl
        return;
      }

      // set error on matchingControl if validation fails
      if (control.value !== matchingControl.value) {
        matchingControl.setErrors({mustMatch: true});
      } else {
        matchingControl.setErrors(null);
      }

    }
  }
}
