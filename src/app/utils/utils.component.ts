import {Component, OnInit} from '@angular/core';
import {Router} from "@angular/router";
import {AuthentificationService} from "../security/services/authentification.service";
import {AppService} from "../app.service";
import {ConstantsService} from '../constantes';
import {AuthentificationWebService} from '../security/web/authentification.web';
import {AnecdoteService} from '../web/anecdote.service';
import {CommentService} from '../web/comment.service';

@Component({
  selector: 'app-utils',
  templateUrl: './utils.component.html',
  styleUrls: ['./utils.component.scss']
})
export class UtilsComponent implements OnInit {

  private _administrateur: number = 0;
  private _administrateurSub;
  private _imageAssetUrl : string = '';
  private _userName: string = '';

  get administrateur(): number {
    return this._administrateur;
  }

  get imageAssetUrl(): any {
    return this._imageAssetUrl;
  }

  get username(): any {
    return this._userName;
  }


  //créer variable Subject  this.appService.administrateur

  constructor(private router: Router, private authentificationService: AuthentificationService,
              private appService: AppService, private constants: ConstantsService,
              private authentificationWebService : AuthentificationWebService, private anecdoteService: AnecdoteService,
              private commentService: CommentService) {

    this._userName = authentificationService.getUsernameFromJwtToken(localStorage.getItem('token'));
    this._imageAssetUrl = constants.imagesAssetsUrl;
    this._administrateurSub = this.appService.administrateurSubject.subscribe(
      (admin: number) => {
        this._administrateur = admin;
      }
    );
    this.appService.emitAdminSubject();
  }

  ngOnInit() {
  }

  /**
   * Se déconnecter via la suppression du token dans le localStorage et la redirection du login.
   */
  onLogOut() {
    this.authentificationService.logOut();
    this.appService.administrateur = 0;
    this.appService.emitAdminSubject();
    this.appService.setUser(null);
    this.router.navigateByUrl('/login');
  }

  /**
   * Retourne à l'acceuil
   */
  onHome() {
    this.anecdoteService.refreshLastAnecdote = this.anecdoteService.refreshLastAnecdote !== true;
    this.anecdoteService.refreshBestAnecdote = this.anecdoteService.refreshBestAnecdote !== true;
    this.commentService.refreshComment = this.commentService.refreshComment !== true;

    this.anecdoteService.emitRefreshLastAnecdoteSubject();
    this.anecdoteService.emitRefreshBestAnecdoteSubject();
    this.commentService.emitRefreshCommentSubject();
    this.appService.job = '';
    this.appService.emitJobSubject();
    this.router.navigateByUrl('/home');
  }

}
