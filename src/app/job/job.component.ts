import {Component, Inject, Injector, Input, OnInit, PLATFORM_ID} from '@angular/core';
import {JobService} from '../web/job.service';
import {AuthentificationService} from '../security/services/authentification.service';
import {AnecdoteService} from '../web/anecdote.service';
import {Anecdote} from '../models/anecdote';
import {CommentComponent} from '../comment/comment.component';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {isPlatformBrowser} from '@angular/common';
import {AnecdoteCategoryService} from '../web/anecdote-category.service';
import {PostAnecdoteComponent} from '../post-anecdote/post-anecdote.component';
import {forEach} from '@angular/router/src/utils/collection';
import {User} from '../userLogisistique/models/user';
import {AppService} from '../app.service';

@Component({
  selector: 'app-job',
  templateUrl: './job.component.html',
  styleUrls: ['./job.component.scss']
})
export class JobComponent implements OnInit {

  @Input() job: String;
  private _jwtTokenSession: string = '';
  private _anecdotes: any[];
  private _anecdoteTemplate: any;
  private _anecdoteCategory: any;
  private _size: number = 5;
  private _user: User = new User();
  private _userSub;
  private _currentPage: number = 0;
  private _pages: any = 0;
  private _refreshAnecdotesSub;

  private modalService: NgbModal;

  get anecdoteCategory(){
    return this._anecdoteCategory;
  }

  get anecdotes() {
    return this._anecdotes;
  }

  get anecdoteTemplate() {
    return this._anecdoteTemplate;
  }

  get currentPage(){
    return this._currentPage;
  }

  get pages(){
    return this._pages;
  }

  constructor(private anecdoteService: AnecdoteService, private jobService: JobService,
              private auth: AuthentificationService, @Inject(PLATFORM_ID) private platformId: Object,
              private injector: Injector, private categoryService: AnecdoteCategoryService,
              private appService: AppService, private anecdoteCategoryService : AnecdoteCategoryService) {

    this._jwtTokenSession = localStorage.getItem('token');

    if (isPlatformBrowser(this.platformId)) {
      this.modalService = this.injector.get(NgbModal);
    }

    this._refreshAnecdotesSub = this.anecdoteService.refreshAnecdoteSubject.subscribe(
      (value: boolean) => {
        this.onGetAnecdotesFromJob();
      }
    );
    this.anecdoteService.emitRefreshAnecdoteSubject();
  }

  ngOnInit(): void {
    this.onGetAnecdoteCategoryFromJob();
    this.onGetAnecdotesFromJob();
    this._userSub = this.appService.userSubject.subscribe(
      (user: any) => {
        this._user = user;
      }
    );
    this.appService.emitUserSubject();
  }

  /**
   * Méthode retournant la catégorie de l'anecdote
   */
  private onGetAnecdoteCategoryFromJob() {
    this.anecdoteCategoryService.getCategoryFromJob(this.job, this._jwtTokenSession)
      .subscribe((value) => {
        this._anecdoteCategory = value;
      }, (error) => {
        console.log(error);
      });
  }

  /**
   * Méthode retournant la liste des anecdotes du métier cible
   */
  onGetAnecdotesFromJob() {
    this.anecdoteService.getAnecdotesFromJob(this.job, this._currentPage, this._size, this._jwtTokenSession)
      .subscribe((value) => {
        this._pages = Array(value.totalPages);
        value.content.forEach(content=>{
          this.onCheckIfNotationIsVisible(content);
        });
        this._anecdotes = value;
        this._anecdoteTemplate = value.content[0];
      }, (error) => {
        console.log(error);
      });
  }

  gotoComments(anecdoteId: number) {
    const modalRef = this.modalService.open(CommentComponent);
    modalRef.componentInstance.anecdoteId = anecdoteId;
  }

  /**
   * Méthode permettant de voter un commentaire
   * @param id
   */
  onVoteAnecdote(status: string, id: any) {
    this.anecdoteService.saveAnecdoteStatus(this.auth.getUsernameFromJwtToken(this._jwtTokenSession), status, id, this._jwtTokenSession)
      .subscribe(resp => {
        this.onGetAnecdotesFromJob();
        console.log(resp);
      }, err => {
        alert(err.error);
        console.log(err.error);
      });
  }

  onSaveAnecdote(categoryId: number) {
    const modalRef = this.modalService.open(PostAnecdoteComponent);
    modalRef.componentInstance.categoryId = categoryId;
    modalRef.componentInstance.username = this.auth.getUsernameFromJwtToken(localStorage.getItem('token'));
  }

  onCheckIfNotationIsVisible(anecdote: any) {
    anecdote.visible = true;
    anecdote.votes.forEach(element => {
      element.forEach(x => {
        if (x.userEmail === this._user.email) {
          anecdote.visible = false;
        }
      });
    });
  }

  gotoPage(i: number) {
    this._currentPage = i;
    this.onGetAnecdotesFromJob();
  }

}
