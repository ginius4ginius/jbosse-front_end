import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from '../../validator/custom-validator';
import {UserService} from '../userLogisistique/services/user.service';
import {UserWebService} from '../userLogisistique/web/user.web.service';
import {AppService} from '../app.service';
import {User} from '../userLogisistique/models/user';

@Component({
  selector: 'app-new-user',
  templateUrl: './new-user.component.html',
  styleUrls: ['./new-user.component.scss']
})
export class NewUserComponent implements OnInit {

  mode = 1;
  user: User = new User();
  userForm: FormGroup;
  fichierACharger: File = null;

  // getters
  get sexe() {
    return this.userForm.get('sexe');
  }

  get username() {
    return this.userForm.get('username');
  }

  get email() {
    return this.userForm.get('email');
  }

  get nom() {
    return this.userForm.get('nom');
  }

  get prenom() {
    return this.userForm.get('prenom');
  }


  get password() {
    return this.userForm.get('password');
  }

  get confirmPassword() {
    return this.userForm.get('confirmPassword');
  }


  constructor(private formBuilder: FormBuilder, private userWebService: UserWebService,
              private appService: AppService) {
  }

  ngOnInit() {

    // initialisation de l userForm
    this.userForm = this.formBuilder.group({
      email: new FormControl('', [Validators.required,
        CustomValidators.emailValidator()]),
      username: new FormControl('', [Validators.required,
        Validators.minLength(3), Validators.maxLength(10)]),
      nom: new FormControl('', [Validators.required,
        Validators.minLength(2), Validators.maxLength(20)]),
      prenom: new FormControl('', [Validators.required,
        Validators.minLength(2), Validators.maxLength(20)]),
      password: new FormControl('', [Validators.required, Validators.minLength(4),
        Validators.maxLength(20)]),
      confirmPassword: new FormControl('', [Validators.required]),
      sexe: new FormControl('', [Validators.required]),
    }, {
      validator: CustomValidators.passwordMatcher('password', 'confirmPassword')
    });

  }

  onSaveUser() {
    this.user = this.userForm.value;

    this.user.actif = false;
    this.user.gVersion = 0;
    this.user.uVersion = 0;
    //persistance
    this.userWebService.saveUser(this.user)
      .subscribe((value) => {
        this.user = value;
        this.mode = 2;
      }, (error) => {
        console.log(error);
      });
  }

  /**
   * Méthode qui affecte le fichier a la variable de type File
   * @param file
   */
  chargerFichier(file: any) {
    this.fichierACharger = file.item(0);
  }


}
