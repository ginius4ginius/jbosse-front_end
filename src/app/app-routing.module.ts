import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {LoginComponent} from "./security/components/login/login.component";
import {MainComponent} from "./main/main.component";
import {AnecdoteComponent} from './anecdote/anecdote.component';
import {CommentComponent} from './comment/comment.component';
import {PostAnecdoteComponent} from './post-anecdote/post-anecdote.component';
import {PostCommentComponent} from './post-comment/post-comment.component';
import {NewUserComponent} from './new-user/new-user.component';

const routes: Routes = [{ path: 'home', component: MainComponent },
  { path: 'login', component: LoginComponent },
  { path: 'anecdote/:id', component: AnecdoteComponent },
  { path: 'comment', component: PostCommentComponent },
  { path: 'saveAnecdote/:id/:username', component: PostAnecdoteComponent },
  { path: 'comments/:id', component: CommentComponent },
  { path: 'newUser', component: NewUserComponent},
  { path: '', redirectTo: 'login', pathMatch: 'full' },
  { path: '**', redirectTo: 'login' }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
