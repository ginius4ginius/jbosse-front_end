import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConstantsService} from '../constantes';
import {Observable} from 'rxjs';
import {Categorie} from '../models/categorie';
import {Anecdote} from '../models/anecdote';

@Injectable({
  providedIn: 'root'
})
export class JobService {


  private _host: string = '';


  constructor(private http: HttpClient, private constants: ConstantsService) {
    this._host = constants.host;
  }

  get host(): string {
    return this._host;
  }

  getJob(jwtToken: string):Observable<Anecdote>{
    return this.http.get<Anecdote>(this._host + '/api/categories/',
      {headers: new HttpHeaders({'Authorization': jwtToken})});
  }

}
