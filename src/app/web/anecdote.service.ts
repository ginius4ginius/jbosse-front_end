import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, Subject} from 'rxjs';
import {ConstantsService} from '../constantes';
import {Anecdote} from '../models/anecdote';
import {User} from '../userLogisistique/models/user';

@Injectable({
  providedIn: 'root'
})
export class AnecdoteService implements OnDestroy{

  private _refreshLastAnecdote:boolean;
  refreshLastAnecdoteSubject = new Subject<boolean>();
  private _refreshBestAnecdote:boolean;
  refreshBestAnecdoteSubject = new Subject<boolean>();
  private _refreshAnecdote:boolean;
  refreshAnecdoteSubject = new Subject<boolean>();

  get refreshLastAnecdote(){
    return this._refreshLastAnecdote;
  }

  set refreshLastAnecdote(b: boolean){
    this._refreshLastAnecdote = b;
  }

  get refreshBestAnecdote(){
    return this._refreshBestAnecdote;
  }

  set refreshBestAnecdote(b: boolean){
    this._refreshBestAnecdote = b;
  }

  get refreshAnecdote(){
    return this._refreshAnecdote;
  }

  set refreshAnecdote(b: boolean){
    this._refreshAnecdote = b;
  }

  emitRefreshLastAnecdoteSubject() {
    this.refreshLastAnecdoteSubject.next(this.refreshLastAnecdote);
  }

  emitRefreshBestAnecdoteSubject() {
    this.refreshBestAnecdoteSubject.next(this.refreshBestAnecdote);
  }

  emitRefreshAnecdoteSubject() {
    this.refreshAnecdoteSubject.next(this.refreshAnecdote);
  }



  constructor(private http: HttpClient, private constants : ConstantsService) { }

  /**
   * Méthode de récupération d'une pagination des 10 dernières anecdotes
   * @param page
   * @param size
   * @param jwtToken
   */
  getLastAnecdotes(page: number, size: number, jwtToken: string):Observable<any> {
    return this.http.get(this.constants.host+"/api/lastAnecdotes?page=" + page + "&size=" + size,
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Méthode de récupération d'une pagination des 10 meilleurs anecdotes
   * @param page
   * @param size
   * @param jwtToken
   */
  getBestAnecdotes(page: number, size: number, jwtToken: string):Observable<any> {
    return this.http.get(this.constants.host+"/api/bestAnecdotes?page=" + page + "&size=" + size,
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Méthode qui retourne une liste d'anecdotes liées à un métier
   * @param job
   * @param page
   * @param size
   * @param jwtToken
   */
  getAnecdotesFromJob(job: String, page: number, size: number, jwtToken: string):Observable<any> {
    return this.http.get(this.constants.host+"/api/anecdotes?job=" + job + "&page=" + page+"&size=" + size,
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Méthode retournant une anecdote par son id
   * @param _id
   * @param _jwtTokenSession
   */
  getAnecdotesFromId(_id: any, _jwtTokenSession: string):Observable<any>  {
    return this.http.get(this.constants.host+"/api/anecdote?id=" + _id ,
      {headers: new HttpHeaders({'Authorization': _jwtTokenSession})})
  }

  /**
   * Méthode permettant de sauvegarder un status d'un commentaire ciblé
   * @param username
   * @param status
   * @param id
   * @param _jwtTokenSession
   */
  saveAnecdoteStatus(username: string, status: string, id: any, _jwtTokenSession: string) {
    return this.http.post<any>(this.constants.host+"/api/anecdoteStatus?username="+username+"&status=" +status+"&id="+id,null,
      {headers: new HttpHeaders({'Authorization': _jwtTokenSession}),observe: "response"})
  }

  /**
   * Méthode permettant de sauvegarder une anecdote
   * @param anecdote
   */
  saveAnecdote(anecdote: Anecdote){
    return this.http.post<Anecdote>(this.constants.host+"/api/anecdote",
      anecdote, {headers: new HttpHeaders({'Authorization': localStorage.getItem('token')})});
  }

  ngOnDestroy(): void {
    this.refreshLastAnecdoteSubject.unsubscribe();
    this.refreshBestAnecdoteSubject.unsubscribe();
    this.refreshAnecdoteSubject.unsubscribe();
  }
}
