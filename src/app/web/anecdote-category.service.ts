import {Injectable} from '@angular/core';
import {ConstantsService} from '../constantes';
import {Categorie} from '../models/categorie';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AnecdoteCategoryService {

  private _host: string = '';


  constructor(private http: HttpClient, private constants: ConstantsService) {
    this._host = constants.host;
  }

  get host(): string {
    return this._host;
  }

  getCategories(jwtToken: string):Observable<Categorie[]>{
    return this.http.get<Array<Categorie>>(this._host + '/api/categories/',
      {headers: new HttpHeaders({'Authorization': jwtToken})});
  }

  getCategoryFromJob(job: String, jwtToken: string): Observable<any> {
    return this.http.get<Categorie>(this._host + '/api/category?label=' + job,
      {headers: new HttpHeaders({'Authorization': jwtToken})});
  }
}
