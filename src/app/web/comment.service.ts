import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConstantsService} from '../constantes';
import {Observable, Subject} from 'rxjs';
import {Anecdote} from '../models/anecdote';
import {Comment} from '../models/comment';

@Injectable({
  providedIn: 'root'
})
export class CommentService implements OnDestroy{

  private _host: string = '';

  private  _refreshComment:boolean;
  refreshCommmentSubject = new Subject<boolean>();

  get refreshComment(){
    return this._refreshComment;
  }

  set refreshComment(b: boolean){
    this._refreshComment = b;
  }

  emitRefreshCommentSubject() {
    this.refreshCommmentSubject.next(this._refreshComment);
  }



  constructor(private http: HttpClient, private constants: ConstantsService) {
    this._host = constants.host;
  }

  get host(): string {
    return this._host;
  }

  /**
   * Méthode de récupération d'une liste des 5 derniers commentaires
   * @param jwtToken
   */
  getLastComments(jwtToken: string):Observable<any> {
    return this.http.get(this.constants.host+"/api/lastComments",
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Méthode qui retourne une liste des commentaires liés à un anecdote
   * @param id
   * @param size
   * @param jwtToken
   */
  getCommentsFromJob(id: number, page:number,size: number, jwtToken: string):Observable<any> {
    return this.http.get(this.constants.host+"/api/comments?id=" + id + "&page="+page+"&size=" + size,
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Méthode permettant de sauvegarder un status de l'anecdote ciblé
   * @param username
   * @param status
   * @param id
   * @param _jwtTokenSession
   */
  /**
   * Méthode permettant de sauvegarder un status de l'anecdote ciblé
   * @param username
   * @param status
   * @param id
   * @param _jwtTokenSession
   */
  saveCommentStatus(username: string, status: string, id: any, _jwtTokenSession: string) {
    return this.http.post<any>(this.constants.host+"/api/commentStatus?username="+username+"&status=" +status+"&id="+id,null,
      {headers: new HttpHeaders({'Authorization': _jwtTokenSession}),observe: "response"})
  }

  /**
   * Méthode permettant de sauvegarder un commentaire
   * @param comment
   */
  saveComment(comment: Comment){
    return this.http.post<Comment>(this.constants.host+"/api/comment",
      comment, {headers: new HttpHeaders({'Authorization': localStorage.getItem('token')})});
  }

  ngOnDestroy(): void {
    this.refreshCommmentSubject.unsubscribe();
  }

}
