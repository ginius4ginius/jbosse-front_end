import { Component, OnInit } from '@angular/core';
import {Anecdote} from '../models/anecdote';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {AuthentificationService} from '../security/services/authentification.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {Comment} from '../models/comment';
import {CommentService} from '../web/comment.service';
import {User} from '../userLogisistique/models/user';

@Component({
  selector: 'app-post-comment',
  templateUrl: './post-comment.component.html',
  styleUrls: ['./post-comment.component.scss']
})
export class PostCommentComponent implements OnInit {

  private _username: string = '';
  private _anecdoteId: number;
  private _comment: Comment = new Comment();
  private _anecdote: Anecdote = new Anecdote();
  private _commentForm: FormGroup;
  private _user:User = new User();

  // getters
  get description() {
    return this._commentForm.get('description');
  }

  get commentForm(){
    return this._commentForm;
  }

  get anecdoteId(){
    return this._anecdoteId;
  }

  set anecdoteId(num: number){
    this._anecdoteId =num;
  }

  get username(){
    return this._username;
  }

  set username(str: string){
    this._username =str;
  }

  constructor(private auth: AuthentificationService, private commentService: CommentService
    , private formBuilder: FormBuilder, private modalService: NgbModal) { }

  ngOnInit() {

    // initialisation du formulaire
    this._commentForm = this.formBuilder.group({
      description: new FormControl('', [Validators.required,
        Validators.minLength(0), Validators.maxLength(2000)])
    });

  }

  /**
   * Méthode permettant de poster un commentaire
   */
  onSaveComment() {
    this._comment = this._commentForm.value;

    this._user.username = this._username;
    this._user.sexe = 'HOMME';
    this._user.dateCreation = new Date();
    this._comment.user = this._user;

    this._anecdote.id = this.anecdoteId;
    this._comment.anecdote = this._anecdote;
    this._comment.publishedAt = new Date();

    this.commentService.saveComment(this._comment)
      .subscribe(resp => {
        this.commentService.refreshComment = this.commentService.refreshComment !== true;
        this.commentService.emitRefreshCommentSubject();
        this.onCloseModal();
        console.log(resp);
      }, err => {
        alert(err.error);
        console.log(err.error);
      });
  }

  onCloseModal() {
    this.modalService.dismissAll();
  }

}
