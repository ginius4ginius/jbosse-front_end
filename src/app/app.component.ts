import {Component} from '@angular/core';
import {AppService} from "./app.service";
import {TranslateService} from "@ngx-translate/core";
import {Router} from "@angular/router";
import {AuthentificationService} from "./security/services/authentification.service";
import {UserWebService} from "./userLogisistique/web/user.web.service";
import {User} from "./userLogisistique/models/user";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {


  private _warningErrorMessage: any = null;
  private _dangerErrorMessage: any = null;
  private _user: User = new User();
  private _administrateur: number = 0;
  private _roles: Array<any> = null;
  private _username: string = '';

  //variable subscribe aux subjects de AppService
  private _warningErrorMessageSub;
  private _dangerErrorMessageSub;
  private _userSub;
  private _administrateurSub;
  private _rolesSub;
  private _usernameSub;

  get warningErrorMessage(): any {
    return this._warningErrorMessage;
  }

  set warningErrorMessage(value) {
    this._warningErrorMessage = value;
  }

  get dangerErrorMessage(): any {
    return this._dangerErrorMessage;
  }

  set dangerErrorMessage(value: any) {
    this._dangerErrorMessage = value;
  }

  get user(): User {
    return this._user;
  }

  set user(value: User) {
    this._user = value;
  }


  get administrateur(): number {
    return this._administrateur;
  }


  constructor(private appService: AppService, private translateService: TranslateService, private router: Router,
              private authentificationService: AuthentificationService, private userWebService: UserWebService) {
    translateService.setDefaultLang('fr');
    //this.onLogOut();
  }


  ngOnInit(): void {
    //initialisation des écoutes des Subjects de AppService
    this._administrateurSub = this.appService.administrateurSubject.subscribe(
      (admin: number) => {
        this._administrateur = admin;
      }
    );
    this._rolesSub = this.appService.rolesSubject.subscribe(
      (roles: Array<any>) => {
        this._roles = roles;
      }
    );
    this._usernameSub = this.appService.usernameSubject.subscribe(
      (username: string) => {
        this._username = username;
      }
    );
    this._warningErrorMessageSub = this.appService.warningErrorMessageSubject.subscribe(
      (message: any) => {
        this._warningErrorMessage = message;
      }
    );
    this._userSub = this.appService.userSubject.subscribe(
      (user: any) => {
        this._user = user;
      }
    );
    this._dangerErrorMessageSub = this.appService.dangerErrorMessageSubject.subscribe(
      (message: any) => {
        this._dangerErrorMessage = message;
      }
    );
    this.appService.emitWERRSubject();
    this.appService.emitDERRSubject();
    this.appService.emitUserSubject();
    this.appService.emitAdminSubject();
    this.appService.emitRolesSubject();
    this.appService.emitUserNameSubject();

  }


  /**
   * Se déconnecter via la suppression du token dans le localStorage et la redirection du login.
   */
  onLogOut() {
    this.authentificationService.logOut();
    this.appService.administrateur = 0;
    this.appService.emitAdminSubject();
    this.appService.setUser(null);
    this.router.navigateByUrl('/login');
  }

  /**
   * Méthode permettant de changer la langue de l'application
   * @param language
   */
  switchLanguage(language: string) {
    this.translateService.use(language);
  }


}
