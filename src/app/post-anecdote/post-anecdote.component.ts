import {Component, OnInit} from '@angular/core';
import {AuthentificationService} from '../security/services/authentification.service';
import {AnecdoteService} from '../web/anecdote.service';
import {Anecdote} from '../models/anecdote';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AnecdoteCategorie} from '../models/anecdoteCategorie';
import {User} from '../userLogisistique/models/user';

@Component({
  selector: 'app-post-anecdote',
  templateUrl: './post-anecdote.component.html',
  styleUrls: ['./post-anecdote.component.scss']
})
export class PostAnecdoteComponent implements OnInit {

  private _username: string = '';
  private _categoryId: number;
  private _anecdote: Anecdote;
  private _anecdoteForm: FormGroup;
  private _user:User = new User();
  private _anecdoteCategorie: AnecdoteCategorie = new AnecdoteCategorie();

  // getters
  get description() {
    return this._anecdoteForm.get('description');
  }

  get anecdoteForm(){
    return this._anecdoteForm;
  }

  get categoryId(){
    return this._categoryId;
  }

  set categoryId(num: number){
    this._categoryId =num;
  }

  get username(){
    return this._username;
  }

  set username(str: string){
    this._username =str;
  }


  constructor(private auth: AuthentificationService, private anecdoteService: AnecdoteService
    , private formBuilder: FormBuilder, private modalService: NgbModal) {
  }

  ngOnInit() {

    // initialisation du formulaire
    this._anecdoteForm = this.formBuilder.group({
      description: new FormControl('', [Validators.required,
        Validators.minLength(0), Validators.maxLength(2000)])
    });

  }

  /**
   * Méthode permettant de poster un anecdote
   */
  onSaveAnecdote() {
    this._anecdote = this._anecdoteForm.value;

    this._user.username = this._username;
    this._user.sexe = 'HOMME';
    this._user.dateCreation = new Date();
    this._anecdote.user = this._user;

    this._anecdoteCategorie.id = this._categoryId;
    this._anecdote.category = this._anecdoteCategorie;
    this._anecdote.publishedAt = new Date();
    this.anecdoteService.saveAnecdote(this._anecdote)
      .subscribe(resp => {
        this.onCloseModal();
        console.log(resp);
      }, err => {
        alert(err.error);
        console.log(err.error);
      });
  }

  onCloseModal() {
    this.modalService.dismissAll();
  }
}
