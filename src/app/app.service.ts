import {Injectable, OnDestroy} from "@angular/core";
import {Subject} from "rxjs";
import {User} from "./userLogisistique/models/user";

@Injectable()
export class AppService implements OnDestroy{

  private _user: User = new User();
  private warningErrorMessage: any = null;
  private dangerErrorMessage: any = null;
  private _administrateur: number = 0;
  private _roles: Array<any> = null;
  private _username: string = '';
  job: string='';

  warningErrorMessageSubject = new Subject<any[]>();
  dangerErrorMessageSubject = new Subject<any[]>();
  userSubject = new Subject<User>();
  administrateurSubject = new Subject<number>();
  rolesSubject = new Subject<any[]>();
  usernameSubject = new Subject<string>();
  jobSubject = new Subject<string>();

  constructor() {
  }

  get administrateur(): number {
    return this._administrateur;
  }

  set administrateur(value: number) {
    this._administrateur = value;
  }

  get roles(): Array<any> {
    return this._roles;
  }

  set roles(value: Array<any>) {
    this._roles = value;
  }

  get username(): string {
    return this._username;
  }

  set username(value: string) {
    this._username = value;
  }

  emitJobSubject() {
    this.jobSubject.next(this.job);
  }

  emitWERRSubject() {
    this.warningErrorMessageSubject.next(this.warningErrorMessage);
  }

  emitDERRSubject() {
    this.dangerErrorMessageSubject.next(this.dangerErrorMessage);
  }

  emitUserSubject() {
    this.userSubject.next(this._user);
  }

  emitAdminSubject() {
    this.administrateurSubject.next(this._administrateur);
  }

  emitRolesSubject() {
    this.rolesSubject.next(this._roles);
  }

  emitUserNameSubject() {
    this.usernameSubject.next(this._username);
  }

  showWarningErrorMessage(msg: any) {
    this.warningErrorMessage = msg;
    this.emitWERRSubject();
  }

  showDangerErrorMessage(msg: any) {
    this.dangerErrorMessage = msg;
    this.emitDERRSubject();
  }

  clearErrorMessage() {
    this.warningErrorMessage = null;
    this.dangerErrorMessage = null;
    this.emitWERRSubject();
    this.emitDERRSubject();
  }

  setUser(user: any){
    this._user = user;
    this.emitUserSubject();
  }

  /**
   * Méthode permettant d'initialiser les variables requises pour la gestion du menu et de ses authorisation d'accés.
   * @param userRoles
   * @param username
   */
  setAuThentification(userRoles: Array<any>, username: string) {
    this.roles = userRoles;
    this.username = username;
    this.emitRolesSubject();
    this.emitUserNameSubject();

    //traitement du tableau des roles de l'utilisateur authentifié.
    for (let r of this._roles) {
      if (r.authority == "ADMIN") {
        this.administrateur = 1;
        this.emitAdminSubject();
      }
    }
  }

  ngOnDestroy(): void {
    this.warningErrorMessageSubject.unsubscribe();
    this.dangerErrorMessageSubject.unsubscribe();
    this.userSubject.unsubscribe();
    this.administrateurSubject.unsubscribe();
    this.rolesSubject.unsubscribe();
    this.usernameSubject.unsubscribe();
    this.jobSubject.unsubscribe();
  }


}
