import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {AuthentificationService} from './security/services/authentification.service';
import {LoginComponent} from './security/components/login/login.component';
import {MainComponent} from './main/main.component';
import {HttpClient, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {TranslateLoader, TranslateModule} from '@ngx-translate/core';
import {TranslateHttpLoader} from '@ngx-translate/http-loader';
import {AppService} from './app.service';
import {UserWebService} from './userLogisistique/web/user.web.service';
import {RoleWebService} from './userLogisistique/web/role.web.service';
import {AuthentificationWebService} from './security/web/authentification.web';
import { UtilsComponent } from './utils/utils.component';
import {ConstantsService} from './constantes';
import { JobComponent } from './job/job.component';
import {CommentService} from './web/comment.service';
import { AnecdoteComponent } from './anecdote/anecdote.component';
import {NgbActiveModal, NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { CommentComponent } from './comment/comment.component';
import { PostAnecdoteComponent } from './post-anecdote/post-anecdote.component';
import { PostCommentComponent } from './post-comment/post-comment.component';
import { NewUserComponent } from './new-user/new-user.component';

export function createTranslateLoader(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    LoginComponent,
    UtilsComponent,
    JobComponent,
    AnecdoteComponent,
    CommentComponent,
    PostAnecdoteComponent,
    PostCommentComponent,
    NewUserComponent,
  ],
  imports: [
    NgbModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: (createTranslateLoader),
        deps: [HttpClient]
      }
    })
  ],
  providers: [AuthentificationService, AuthentificationWebService, AppService, UserWebService, RoleWebService,
    ConstantsService, CommentService],
  bootstrap: [AppComponent]
})
export class AppModule { }
