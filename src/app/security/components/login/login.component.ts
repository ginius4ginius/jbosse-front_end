import {Component, Inject, Injector, OnInit, PLATFORM_ID} from '@angular/core';
import {AuthentificationService} from '../../services/authentification.service';
import {Router} from '@angular/router';
import {AuthentificationWebService} from '../../web/authentification.web';
import {AppService} from '../../../app.service';
import {UserWebService} from '../../../userLogisistique/web/user.web.service';
import {User} from '../../../userLogisistique/models/user';
import {isPlatformBrowser} from '@angular/common';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {CommentComponent} from '../../../comment/comment.component';
import {NewUserComponent} from '../../../new-user/new-user.component';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  private _mode: number = 0;
  private modalService: NgbModal;

  get mode(): any {
    return this._mode;
  }

  constructor(private authService: AuthentificationService, private router: Router,
              private appService: AppService, private userWebService: UserWebService,
              private authWebService: AuthentificationWebService,
              @Inject(PLATFORM_ID) private platformId: Object, private injector: Injector) {

    if (isPlatformBrowser(this.platformId)) {
      this.modalService = this.injector.get(NgbModal);
    }

  }

  ngOnInit() {
  }

  /**
   * Méthode permettant de s'authentifier
   * @param formData
   */
  onLogin(formData) {
    this.authWebService.login(formData)
      .subscribe(resp => {
        let jwtToken = resp.headers.get('Authorization');
        this.authService.saveJwtToken(jwtToken);
        this.appService.setAuThentification(this.authService.roles, this.authService._userName);
        this.userWebService.getUserByUsername(this.authService._userName, jwtToken).subscribe(resp => {
          this.appService.setUser(resp);

        }, err => {
          console.log(err);
        });
        this.router.navigateByUrl('/home');

      }, err => {
        this._mode = 1;
      });

  }

  goToNewUser() {
    const modalRef = this.modalService.open(NewUserComponent);
  }

}
