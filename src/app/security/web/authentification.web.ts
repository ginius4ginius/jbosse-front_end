import {Injectable} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {ConstantsService} from '../../constantes';

/**
 * Classe de ressources HTTP
 */
@Injectable()
export class AuthentificationWebService {

  private _host: string = '';

  constructor(private http: HttpClient, private constants: ConstantsService) {
    this._host = constants.host;
  }

  get host(): string {
    return this._host;
  }


/**
 * Appel de la ressource POST.login
 * @param user
 */
login(user) {
  return this.http.post(this._host+"/login", user, {observe: "response"})
}

}
