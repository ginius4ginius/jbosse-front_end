import {Injectable} from "@angular/core";
import {JwtHelperService} from '@auth0/angular-jwt';

@Injectable()
export class AuthentificationService {

  public roles: Array<any>;
  public _userName : string = '';
  private _roleUser: string = 'USER';
  private _roleMod: string = 'MOD';
  private _roleAdmin: string = 'ADMIN';
  private _roleSuperAdmin: string = 'SUPER_ADMIN';

  get username(){
    return this._userName;
  }

  constructor() {
    this.roles = null;
  }

  logOut() {
    localStorage.removeItem('token');
  }

  /**
   * Méthode qui sauvegarde le token JWT dans le local storage
   * et attribue les roles de l'utilisateur
   * @param jwtToken
   */
  saveJwtToken(jwtToken: string) {
    localStorage.setItem('token', jwtToken);
    this.roles = this.getRolesFromJwtToken(jwtToken);
    this._userName = this.getUsernameFromJwtToken(jwtToken);
  }

  /**
   * Récupère les rôle de l'utilisateur via le payload du JWT
   * @param jwtToken
   */
  getRolesFromJwtToken(jwtToken: string) {
    let jwtHelper = new JwtHelperService();
    return jwtHelper.decodeToken(jwtToken).roles;
  }

  /**
   * Récupère le champs sub du JWT soit n'username de l'utilisateur authentifié
   * @param jwtToken
   */
  getUsernameFromJwtToken(jwtToken: string) {
    let jwtHelper = new JwtHelperService();
    return jwtHelper.decodeToken(jwtToken).sub;
  }

  /**
   * Méthode qui parcoure une liste de role et retourne un bouléen si il trouve le role en 2eme argument
   * @param roles
   * @param user
   */
  findRoleType(roles: Array<any>, user: string) {
    let result = false;
    for (let r of roles) {
      if (r.authority == user) result = true;
    }
    return result;
  }

  /**
   * retourne true si UTILISATEUR est dans la liste des rôles
   * @param roles
   */
  isUser(roles: Array<any>) {
    return this.findRoleType(roles, this._roleUser);
  }

  /**
   * retourne true si MODERATEUR est dans la liste des rôles
   * @param roles
   */
  isMod(roles: Array<any>) {
    return this.findRoleType(roles, this._roleMod);
  }

  /**
   * retourne true si ADMINISTRATEUR est dans la liste des rôles
   * @param roles
   */
  isAdmin(roles: Array<any>) {
    return this.findRoleType(roles, this._roleAdmin);
  }

  /**
   * retourne true si SUPER_ADMINISTRATEUR est dans la liste des rôles
   * @param roles
   */
  isSuperAdmin(roles: Array<any>) {
    return this.findRoleType(roles, this._roleSuperAdmin);
  }

}
