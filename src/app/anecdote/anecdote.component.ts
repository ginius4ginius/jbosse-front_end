import {Component, Inject, Injector, OnInit, PLATFORM_ID} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {NgbAccordionConfig, NgbActiveModal, NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AnecdoteService} from '../web/anecdote.service';
import {JobService} from '../web/job.service';
import {AuthentificationService} from '../security/services/authentification.service';
import {CommentService} from '../web/comment.service';
import {isPlatformBrowser} from '@angular/common';
import {CommentComponent} from '../comment/comment.component';
import {forEach} from '@angular/router/src/utils/collection';
import {element} from 'protractor';
import {AppService} from '../app.service';
import {User} from '../userLogisistique/models/user';

@Component({
  selector: 'app-anecdote',
  templateUrl: './anecdote.component.html',
  styleUrls: ['./anecdote.component.scss']
})
export class AnecdoteComponent implements OnInit {

  private _id: any = 0;
  private _jwtTokenSession: string = '';
  private _anecdote: any;
  private _user: User = new User();
  private _userSub;

  private modalService: NgbModal;

  get anecdote() {
    return this._anecdote;
  }

  get id() {
    return this._id;
  }

  set id(id: number) {
    this._id = id;
    this.onGetAnecdote();
  }

  constructor(private route: ActivatedRoute, public activeModal: NgbActiveModal,
              private anecdoteService: AnecdoteService, private config: NgbAccordionConfig,
              private auth: AuthentificationService, @Inject(PLATFORM_ID) private platformId: Object,
              private injector: Injector, private appService: AppService) {

    if (isPlatformBrowser(this.platformId)) {
      this.modalService = this.injector.get(NgbModal);
    }

    config.closeOthers = true;
    config.type = 'white';

    this._jwtTokenSession = localStorage.getItem('token');


  }

  ngOnInit(): void {
    this._userSub = this.appService.userSubject.subscribe(
      (user: any) => {
        this._user = user;
      }
    );
    this.appService.emitUserSubject();
  }

  /**
   * Méthode retournant une anecdote par son id
   */
  onGetAnecdote() {
    this.anecdoteService.getAnecdotesFromId(this._id, this._jwtTokenSession)
      .subscribe((value) => {
        this._anecdote = value;
        this.onCheckIfNotationIsVisible(this._anecdote);
      }, (error) => {
        console.log(error);
      });
  }

  /**
   * Méthode permettant de voter un commentaire
   * @param status
   * @param id
   */
  onVoteAnecdote(status: string, id: any) {
    this.anecdoteService.saveAnecdoteStatus(this.auth.getUsernameFromJwtToken(this._jwtTokenSession), status, id, this._jwtTokenSession)
      .subscribe(resp => {
        this.onGetAnecdote();
        console.log(resp);
      }, err => {
        alert(err.error);
        console.log(err.error);
      });
  }

  gotoComments(id: any) {
    const modalRef = this.modalService.open(CommentComponent);
    modalRef.componentInstance.anecdoteId = id;
  }

  onCheckIfNotationIsVisible(anecdote: any) {
    anecdote.visible = true;
    anecdote.votes.forEach(element => {
      element.forEach(x => {
        if (x.userEmail === this._user.email) {
          anecdote.visible = false;
        }
      });
    });
  }

}
