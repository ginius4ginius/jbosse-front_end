
export class Anecdote {

  id:number;
  publishedAt:Date;
  description:string='';
  category:any;
  user:any;
  notes:any[];

}
