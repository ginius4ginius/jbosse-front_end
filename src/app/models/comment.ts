import {Anecdote} from './anecdote';
import {User} from '../userLogisistique/models/user';

export class Comment {

  id:number;
  description:string='';
  publishedAt: Date;
  anecdote: Anecdote;
  user: User;


}
