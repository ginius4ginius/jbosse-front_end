import {Component, Inject, Injector, OnInit, PLATFORM_ID} from '@angular/core';
import {CommentService} from '../web/comment.service';
import {AuthentificationService} from '../security/services/authentification.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {PostCommentComponent} from '../post-comment/post-comment.component';
import {isPlatformBrowser} from '@angular/common';
import {User} from '../userLogisistique/models/user';
import {AppService} from '../app.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.scss']
})
export class CommentComponent implements OnInit {

  private _anecdoteId= 0;
  private _jwtTokenSession='';
  private _comments:Array<any> = [];

  private modalService: NgbModal;

  private _size : number = 5;
  private _pageCommentsContent: any;
  private _pagesComments: any = 0;
  private _currentPage: number =0;

  private _user: User = new User();
  private _userSub;

  get currentPage(){
    return this._currentPage;
  }

  get pagesComments(){
    return this._pagesComments;
  }

  get comments(){
    return this._comments;
  }

  get pageCommentsContent(){
    return this._pageCommentsContent;
  }

  get anecdoteId(){
    return this._anecdoteId
  }

  set anecdoteId(id: number){
    this._anecdoteId = id;
  }


  constructor(private commentService: CommentService, private auth: AuthentificationService,
              @Inject(PLATFORM_ID) private platformId: Object, private injector: Injector,
              private appService: AppService) {

    if (isPlatformBrowser(this.platformId)) {
      this.modalService = this.injector.get(NgbModal);
    }

    this._jwtTokenSession = localStorage.getItem('token');
  }

  ngOnInit() {
    this.doSearchComments(this._anecdoteId);
    this._userSub = this.appService.userSubject.subscribe(
      (user: any) => {
        this._user = user;
      }
    );
    this.appService.emitUserSubject();
  }

  doSearchComments(anecdoteId: any) {
    this.commentService.getCommentsFromJob(anecdoteId,this._currentPage,this._size,this._jwtTokenSession)
      .subscribe(resp => {
        console.log(resp.content);
        resp.content.forEach(content=>{
          this.onCheckIfNotationIsVisible(content);
        });
        this._comments = resp;
      }, err => {
        alert(err.error);
        console.log(err.error);
      });
  }

  /**
   * Méthode permettant de sauvegarder une notation d'un commentaire
   * @param status
   * @param id
   */
  onVoteComment(status: string, id: any,anecdoteId: any) {
    this.commentService.saveCommentStatus(this.auth.getUsernameFromJwtToken(this._jwtTokenSession), status, id, this._jwtTokenSession)
      .subscribe(resp => {
        this.doSearchComments(anecdoteId);
        console.log(resp);
      }, err => {
        alert(err.error);
        console.log(err.error);
      });
  }

  gotoPage(i: number) {
    this._currentPage = i;
    this.doSearchComments(this._anecdoteId);
  }

  onSaveComment() {
    const modalRef = this.modalService.open(PostCommentComponent);
    modalRef.componentInstance.anecdoteId = this._anecdoteId;
    modalRef.componentInstance.username = this.auth.getUsernameFromJwtToken(localStorage.getItem('token'));
  }

  onCheckIfNotationIsVisible(comment: any) {
    comment.visible = true;
    comment.votes.forEach(element => {
      element.forEach(x => {
        if (x.userEmail === this._user.email) {
          comment.visible = false;
        }
      });
    });
  }
}
