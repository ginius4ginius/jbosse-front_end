import {Component, Inject, Injector, OnInit, PLATFORM_ID} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {AuthentificationService} from '../security/services/authentification.service';
import {AppService} from '../app.service';
import { ConstantsService } from '../constantes';
import {AnecdoteCategoryService} from '../web/anecdote-category.service';
import {Categorie} from '../models/categorie';
import {AnecdoteService} from '../web/anecdote.service';
import {CommentService} from '../web/comment.service';
import {NgbModal} from '@ng-bootstrap/ng-bootstrap';
import {AnecdoteComponent} from '../anecdote/anecdote.component';
import {isPlatformBrowser} from '@angular/common';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  private roles: Array<any> = [];
  private _imageAssetUrl : string = '';
  private _categories : Array<Categorie> = [];
  private _comments: Array<any> = [];
  private _jwtTokenSession: string = '';
  private _jobSub;
  private _job: string = '';
  private _refreshLastAnecdoteSub;
  private _refreshBestAnecdoteSub;
  private _refreshCommentSub;

  private _lasts: any;
  private _pageLasts: any = 0;
  private _bests: any;
  private _pageBests: any = 0;

  private _bestCurrentPage: number = 0;
  private _lastCurrentPage: number = 0;
  private _size: number = 1;

  private modalService: NgbModal;


  get job(): any {
    return this._job;
  }

  get imageAssetUrl(): any {
    return this._imageAssetUrl;
  }

  get categories(): any {
    return this._categories;
  }

  get comments(): any {
    return this._comments;
  }

  get lasts(): any {
    return this._lasts;
  }

  get bests(): any {
    return this._bests;
  }

  get bestCurrentPage(): any {
    return this._bestCurrentPage;
  }

  get lastCurrentPage(): any {
    return this._lastCurrentPage;
  }

  get size(): any {
    return this._size;
  }

  get pageLasts(): any {
    return this._pageLasts;
  }

  get pageBests(): any {
    return this._pageBests;
  }

  constructor(private router: Router, private authentificationService: AuthentificationService,
              private contant: ConstantsService, private categoryService : AnecdoteCategoryService,
              private anecdoteService: AnecdoteService, private appService: AppService,
              private commentService : CommentService, private route: ActivatedRoute,
              @Inject(PLATFORM_ID) private platformId: Object, private injector: Injector) {

    if(isPlatformBrowser(this.platformId)){
      this.modalService = this.injector.get(NgbModal);
    }
    this._imageAssetUrl = contant.imagesAssetsUrl;
    this._jwtTokenSession = localStorage.getItem('token');
    if (this._jwtTokenSession != null) {
      this.roles = this.authentificationService.getRolesFromJwtToken(this._jwtTokenSession);
    }
    if (this._jwtTokenSession == null || !this.authentificationService.isUser(this.roles)) {
      router.navigateByUrl('/login');
    }
    this._jobSub = this.appService.jobSubject.subscribe(
      (value: string) => {
        this._job = value;
      }
    );
    this.appService.emitJobSubject();

    this._refreshLastAnecdoteSub = this.anecdoteService.refreshLastAnecdoteSubject.subscribe(
      (value: boolean) => {
        this.doSearchLastAnecdotes();
      }
    );
    this.anecdoteService.emitRefreshLastAnecdoteSubject();

    this._refreshBestAnecdoteSub = this.anecdoteService.refreshBestAnecdoteSubject.subscribe(
      (value: boolean) => {
        this.doSearchBestAnecdotes();
      }
    );
    this.anecdoteService.emitRefreshLastAnecdoteSubject();

    this._refreshCommentSub = this.commentService.refreshCommmentSubject.subscribe(
      (value: boolean) => {
        this.doSearchLastComments();
      }
    );
    this.commentService.emitRefreshCommentSubject();

  }

  ngOnInit() {
    this.onGetCategories();
    this.doSearchLastAnecdotes();
    this.doSearchBestAnecdotes();
    this.doSearchLastComments();
  }

  /**
   * Méthode retournant la liste des métiers
   */
  onGetCategories(){
    this.categoryService.getCategories(this._jwtTokenSession)
      .subscribe((value) => {
        this._categories = value;
      }, (error) => {
        console.log(error);
      });
  }

  doSearchLastAnecdotes() {
    this.anecdoteService.getLastAnecdotes(this.lastCurrentPage, this.size, this._jwtTokenSession)
      .subscribe((value) => {
        this._pageLasts = Array(value.totalPages);
        this._lasts = value;

      }, (error) => {
        console.log(error);
      });

  }

  doSearchBestAnecdotes() {
    this.anecdoteService.getBestAnecdotes(this.bestCurrentPage, this.size, this._jwtTokenSession)
      .subscribe((value) => {
        this._pageBests = Array(value.totalPages);
        this._bests = value;

      }, (error) => {
        console.log(error);
      });

  }

  doSearchLastComments() {
    this.commentService.getLastComments(this._jwtTokenSession)
      .subscribe((value) => {
        this._comments = value;
      }, (error) => {
        console.log(error);
      });

  }

  gotoPageLast(i: number) {
    this._lastCurrentPage = i;
    this.doSearchLastAnecdotes();
  }

  gotoPageBest(i: number) {
    this._bestCurrentPage = i;
    this.doSearchBestAnecdotes();
  }



  gotoJob(label: string) {
    this._job = label;
  }



  gotoAnecdote(id: number) {
    const modalRef = this.modalService.open(AnecdoteComponent);
    modalRef.componentInstance.id = id;
  }
}
