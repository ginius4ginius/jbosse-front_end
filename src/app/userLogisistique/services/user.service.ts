import {Injectable, OnDestroy} from "@angular/core";
import {HttpClient} from "@angular/common/http";
import {User} from "../models/user";
import {UserWebService} from "../web/user.web.service";
import {Subject} from "rxjs";

@Injectable()
export class UserService implements OnDestroy{

  private _showDashBoard: number= 0;
  private _editUserComponent: number= 0;
  showDashBoardSubject = new Subject<number>();
  editUserComponentSubject = new Subject<number>();

  constructor() {
  }


  get showDashBoard(): number {
    return this._showDashBoard;
  }

  set showDashBoard(value: number) {
    this._showDashBoard = value;
  }

  get editUserComponent(): number {
    return this._editUserComponent;
  }

  set editUserComponent(value: number) {
    this._editUserComponent = value;
  }

  emitshowDashBoardSubject() {
    this.showDashBoardSubject.next(this._showDashBoard);
  }

  emiteditUserComponentSubject() {
    this.editUserComponentSubject.next(this.editUserComponent);
  }

  cancelUpdate(){
    //afficher le dashboard
    this.editUserComponent=0;
    this.showDashBoard=1;
    this.emiteditUserComponentSubject();
    this.emitshowDashBoardSubject();
  }

  ngOnDestroy(): void {
    this.showDashBoardSubject.unsubscribe();
    this.editUserComponentSubject.unsubscribe();
  }

}
