import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Role} from '../models/role';
import {Observable} from 'rxjs';
import {AuthentificationWebService} from "../../security/web/authentification.web";

@Injectable()
export class RoleWebService {

  private rolesList: Array<Role> = [];
  private host: string = 'http://localhost:8082';

  constructor(private http: HttpClient) {
  }

  getRoles(jwtToken: string):Observable<Role[]> {
    return this.http.get<Array<Role>>(this.host+'/account/roles',
      {headers: new HttpHeaders({'Authorization': jwtToken})});
  }
}
