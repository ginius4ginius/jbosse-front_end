import {Injectable} from '@angular/core';
import {User} from 'src/app/userLogisistique/models/user';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {ConstantsService} from '../../constantes';

@Injectable({
  providedIn: 'root'
})
export class UserWebService {

  private _host: string = '';

  constructor(private http: HttpClient, private constants: ConstantsService) {
    this._host =constants.host;
  }


  get host(): string {
    return this._host;
  }

  /**
   * Ressource REST récupération des utilisateurs par mc
   * @param motCle
   * @param page
   * @param size
   * @param jwtToken
   */
  getUsers(motCle: String, page: number, size: number, jwtToken: string) {
    return this.http.get(this._host + '/account/findUsers?mc=' + motCle + '&size=' + size + '&page=' + page,
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Ressource REST récupération des utilisateurs par mc et filtre selon activation de l'utilisateur
   * @param motCle
   * @param page
   * @param size
   * @param jwtToken
   */
  getUsersByActifFilter(motCle: String, page: number, size: number, jwtToken: string, actif : string) {
    return this.http.get(this._host + '/account/findUsersActifFilter?mc=' + motCle + '&size=' + size + '&page=' + page
      + '&actif=' + actif,
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Ressource REST sauvegarde de l'utilisateur
   * @param user
   * @param jwtToken
   */
  saveUser(user: User) {
    return this.http.post<User>(this._host + '/account/saveUser',
      user)
  }

  /**
   * Ressource REST de la récupération d'un utilisateur par son email
   * @param id
   */
  getUserByEmail(email: string, jwtToken: string) : any {
    return this.http.get<User>(this._host + '/account/user/' + email,
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Ressource REST de la récupération d'un utilisateur par son username
   * @param username
   * @param jwtToken
   */
  getUserByUsername(username: string, jwtToken: string) : any {
    return this.http.get<User>(this._host + '/account/userByUsername/' + username,
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Ressource REST mise à jour de l'utilisateur
   * @param user
   * @param jwtToken
   */
  updateUser(user: User, jwtToken: string) {
    user.roles[0].libelle = user.roles[0].libelle.replace(/\s+/g, '_');
    return this.http.put<User>(this._host + '/account/user', user,
    {headers: new HttpHeaders({'Authorization': jwtToken})})
  }

  /**
   * Méthode d'appel sur service pour sauvegarder le fichier
   * @param fichierAEnvoyer
   * @param nomFichier
   * @param jwtToken
   */
  envoiFichier(fichierAEnvoyer: File, nomFichier : string){
    const url = this._host + '/account/photoUser';
    const formData: FormData = new FormData();
    formData.append('file', fichierAEnvoyer, nomFichier);
    return this.http
      .post(url, formData)
  }


  getUsersInfo(jwtToken: string){
    return this.http.get(this._host + '/account/usersInfo',
      {headers: new HttpHeaders({'Authorization': jwtToken})})
  }
}
