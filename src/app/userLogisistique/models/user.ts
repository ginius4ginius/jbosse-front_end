
export class User {

  email:string="";
  username:string="";
  nom:string="";
  prenom:string="";
  sexe:string="";
  dateCreation:Date;
  dateUtilisation:Date;
  actif:Boolean;
  photo:string="";
  password:string="";
  confirmPassword="";
  gVersion:number;
  uVersion:number;
  roles: any;

}
